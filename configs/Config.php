<?php

namespace Configs;

use Repositories\UserRepository;
use Repositories\UserRepositoryInterface;

/**
 * Класс, содержащий в себе конфигурационные параметры приложения
 */
class Config
{
    /**
     * @var array необходимые параметры для подключения к БД
     */
    public static $dbConfig = [];

    /**
     * Домашняя страница URL
     *
     * @var string
     */
    public static $defaultUrl = '/main';

    /**
     * Путь до папки с шаблонами
     *
     * @var string
     */
    public static $pathToTemplates = '';

    /**
     * Массив псевдонимов для DI
     *
     * @var string[]
     */
    public static $aliases = [];
}

/**
 * Параметры подключения к БД MySql
 */

Config::$dbConfig = [
    'host'     => 'mysql',
    'login'    => 'root',
    'password' => 'root',
    'db_name'  => 'financial_transactions_test',
    'charset'  => 'utf8'
];

Config::$aliases = [
    UserRepositoryInterface::class => UserRepository::class
];

Config::$pathToTemplates = dirname(__DIR__) . '/templates/';

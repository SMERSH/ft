<?php

declare(strict_types=1);

namespace Controllers;

use Core\Auth;
use Core\Session;
use Core\SimpleController;
use Core\View;
use Forms\FormLogin;
use Forms\FormWithdrawal;
use Models\User;
use Services\UserService;

class MainController extends SimpleController
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var Session
     */
    private $session;

    /**
     * @var Auth
     */
    private $auth;

    /**
     * MainController constructor.
     * @param View $view
     * @param UserService $userService
     * @param Session $session
     */
    public function __construct(View $view, UserService $userService, Session $session, Auth $auth)
    {
        parent::__construct($view);

        $this->userService = $userService;
        $this->session     = $session;
        $this->auth        = $auth;
    }

    /**
     * Метод вызывает дефолтный экшн
     */
    public function actionDefault()
    {
        $this->runAction('index');
    }

    /**
     * Главная страница
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!$this->auth->check()) {
            return $this->redirect('/main/login');
        }

        $id = (int)$this->session->get('id');
        /** @var User $user */
        $user = $this->userService->getRepository()->findById($id);

        $form = new FormWithdrawal();
        $form->handleRequest($this->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->userService->getRepository()->getDbh()->beginTransaction();
                $this->userService->makeWithdrawal((int)$id, $form->getConvertToPennies());

                $this->userService->getRepository()->getDbh()->commit();

                return $this->redirect();
            } catch (\DomainException $e) {
                $this->userService->getRepository()->getDbh()->rollBack();
                $this->addError($e->getMessage());
            } catch (\Throwable $e) {
                $this->addError('Внутренняя ошибка сервера');
            }
        }

        return $this->view->render('index', [
            'balance' => $user->getBalanceView(),
            'form'    => $form,
            'errors'  => array_merge($form->getErrors(), $this->errors),
        ]);
    }

    /**
     * Авторизация пользователя
     */
    public function actionLogin()
    {
        if ($this->auth->check()) {
            return $this->redirect('/');
        }

        $form = new FormLogin();
        $form->handleRequest($this->getRequest());
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $user = $this->userService->login(
                    $form->getParam('login'),
                    $form->getParam('password')
                );

                $this->session->setParams([
                    'id'    => $user->getId(),
                    'login' => $user->getLogin()
                ]);

                return $this->redirect('/');
            } catch (\DomainException $e) {
                $this->addError($e->getMessage());
            } catch (\Throwable $e) {
                $this->addError('Внутренняя ошибка сервера');
            }
        }

        return $this->view->render('login', [
            'errors'  => array_merge($form->getErrors(), $this->errors),
        ]);
    }

    /**
     * Логаут пользователя
     */
    public function actionLogout()
    {
        if (!$this->auth->check()) {
            return $this->redirect('/main/login');
        }

        $this->session->destroy();

        return $this->redirect('/main/login');
    }
}

<?php

declare(strict_types=1);

namespace Core;

use Core\Session;

/**
 * Приложение является базовым классом для веб-приложений.
 */
class Application
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function run()
    {
        /** @var Router $router */
        $router = $this->container->get(Router::class);

        /** @var Session $session */
        $session = $this->container->get(Session::class);
        $session->init();

        $router->run();
    }
}

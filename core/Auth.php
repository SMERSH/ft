<?php

declare(strict_types=1);

namespace Core;

/**
 * Класс проверки авторизации пользователя
 */
class Auth
{
    /**
     * @var Session
     */
    private $session;

    /**
     * Auth constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * Метод проверки авторизации пользователя
     *
     * @return bool
     */
    public function check(): bool
    {
        if ($this->session->has('id')) {
            return true;
        }

        return false;
    }
}

<?php

declare(strict_types=1);

namespace Core;

/**
 * Автозагрузка классов
 */
class Autoloader
{
    public $namespaceMap = [];
    public static $classMap;

    public function addNamespace($namespace, $dir)
    {
        if (is_dir($dir)) {
            $this->namespaceMap[$namespace] = $dir;
            return true;
        }
        return false;
    }

    public function register()
    {
        spl_autoload_register([$this, 'autoload']);
    }

    protected function autoload($class)
    {
        $pathParts = explode('\\', $class);

        if (is_array($pathParts)) {
            $namespace = ucfirst(array_shift($pathParts));
            if (array_key_exists($namespace, $this->namespaceMap)) {
                $filePath = $this->namespaceMap[$namespace] . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $pathParts).'.php';
                if (!file_exists($filePath)) {
                    return false;
                }

                require_once $filePath;

                return true;
            }
        }

        return false;
    }

    public static function classLoader()
    {
        self::$classMap = require(__DIR__ . '/Classmap.php');

        $autoloader = new Autoloader();
        foreach (self::$classMap as $namespace => $file) {
            $namespace = ucfirst($namespace);
            $autoloader->addNamespace($namespace, $file);
        }

        $autoloader->register();
    }
}

return Autoloader::classLoader();

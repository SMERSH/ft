<?php
/**
 * Массив пространства имен
 */
$baseDir = dirname(__DIR__);

return [
    'core'         => __DIR__,
    'configs'      => $baseDir . DIRECTORY_SEPARATOR . 'configs',
    'controllers'  => $baseDir . DIRECTORY_SEPARATOR . 'controllers',
    'forms'        => $baseDir . DIRECTORY_SEPARATOR . 'forms',
    'models'       => $baseDir . DIRECTORY_SEPARATOR . 'models',
    'repositories' => $baseDir . DIRECTORY_SEPARATOR . 'repositories',
    'services'     => $baseDir . DIRECTORY_SEPARATOR . 'services',
    'widgets'      => $baseDir . DIRECTORY_SEPARATOR . 'widgets',
];

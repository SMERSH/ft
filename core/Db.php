<?php

declare(strict_types=1);

namespace Core;

use Configs\Config;

/**
 * Класс подключения к БД
 */
class Db
{
    /**
     * @var \PDO
     */
    private $dbh;

    public function __construct(Config $config)
    {
        $dbConfig = $config::$dbConfig;

        try {
            $this->dbh = new \PDO(
                "mysql:host=" . $dbConfig['host'] .
                ";dbname=" . $dbConfig['db_name'] .
                ';charset=' . $dbConfig['charset'],
                $dbConfig['login'],
                $dbConfig['password']
            );
        } catch (\PDOException $e) {
            echo 'Ошибка подключения к БД';
        }
    }

    /**
     * Клонирование и десериализация не разрешены для одиночек.
     */
    protected function __clone()
    {
    }

    public function __wakeup()
    {
        throw new \Exception("Cannot unserialize singleton");
    }

    /**
     * @return \PDO
     */
    public function getDbh()
    {
        return $this->dbh;
    }
}

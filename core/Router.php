<?php

declare(strict_types=1);

namespace Core;

use Configs\Config;

/**
 * Класс отвечающий за маршрутизацию
 */
class Router
{
    private $uri;

    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getUri()
    {
        if (is_null($this->uri)) {
            $this->uri = ($_SERVER['REQUEST_URI'] == '/') ? Config::$defaultUrl : $_SERVER['REQUEST_URI'];
        }

        return $this->uri;
    }

    public function run()
    {
        $url_parameters  = explode('/', trim($this->getUri(), '/'));

        // Первый параментр — контроллер.
        $controller_name = ucfirst(array_shift($url_parameters)) . 'Controller';

        // Второй — действие.
        $action = array_shift($url_parameters);
        $action = (is_null($action)) ? 'default' : $action;
        $action = 'action' . ucfirst($action);

        $controller = 'controllers\\' . ucfirst($controller_name);

        $controllerInstance = $this->container->get($controller);
        if (method_exists($controllerInstance, $action)) {
            call_user_func_array([$controllerInstance, $action], $url_parameters);
        } else {
            die('404 not found');
        }
    }
}

<?php

declare(strict_types=1);

namespace Core;

/**
 * Класс для работы с сессией
 */
class Session
{
    /**
     * Метод инициирования сессии
     */
    public function init(): void
    {
        $this->start();
        $this->sessionWriteClose();
    }

    /**
     * Метод запуска сессии
     */
    public function start(): void
    {
        session_start();
    }

    /**
     * Проверить наличие элемента
     *
     * @param string $name
     *
     * @return bool
     */
    public static function has(string $name): bool
    {
        return array_key_exists($name, $_SESSION);
    }

    /**
     * Получить значение
     *
     * @param string $name
     * @param null $default
     *
     * @return mixed|null
     */
    public static function get(string $name, $default = null)
    {
        if (self::has($name)) {
            return $_SESSION[$name];
        }

        return null;
    }

    /**
     * Установить значение
     *
     * @param $name
     * @param $value
     */
    public function set($name, $value): void
    {
        $this->start();
        $_SESSION[$name] = $value;
        $this->sessionWriteClose();
    }

    /**
     * Установить массив значений
     *
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->start();
        foreach ($params as $name => $value) {
            $_SESSION[$name] = $value;
        }
        $this->sessionWriteClose();
    }

    /**
     * Удалить элемент
     *
     * @param $name
     */
    public function remove($name): void
    {
        $this->start();
        if ($this->has($name)) {
            unset($_SESSION[$name]);
        }
        $this->sessionWriteClose();
    }

    /**
     * Получить ID сессии
     *
     * @return string
     */
    public function getId(): string
    {
        return session_id();
    }

    /**
     * Установить ID сессии
     *
     * @param $id
     * @return void
     */
    public function setId($id): void
    {
        session_id($id);
    }

    /**
     * Записывает данные сессии и завершает её
     */
    public function sessionWriteClose(): void
    {
        session_write_close();
    }

    /**
     * Метод уничтожения сессии
     */
    public function destroy(): void
    {
        $this->start();
        session_destroy();
        $this->sessionWriteClose();
    }
}

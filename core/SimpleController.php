<?php

declare(strict_types=1);

namespace Core;

/**
 * Базовый класс контроллера
 */
abstract class SimpleController
{
    /**
     * @var View
     * Объект, который может использоваться для визуализации представлений или файлов просмотра
     */
    protected $view;

    /**
     * Массив ошибок
     *
     * @var array
     */
    public $errors = [];

    /**
     * Абстрактный метод, позволяющий запускать дефолтный экшен
     */
    abstract protected function actionDefault();

    /**
     * SimpleController constructor.
     * @param View $view
     */
    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * Статический метод, позволяет достать из массива $_REQUEST необходимый элемент
     *
     * @param string $param_name
     * @return string|null
     */
    public static function getParam($param_name)
    {
        //проверяем на существование в массиве запрашиваемой переменной
        if (isset($_REQUEST[$param_name])) {
            return $_REQUEST[$param_name];
        }

        return null;
    }

    /**
     * Получить request
     *
     * @return array
     */
    public static function getRequest()
    {
        return $_REQUEST;
    }

    /**
     * Метод, запускающий необходимый экшен
     *
     * @param $action
     * @return string|null
     */
    protected function runAction($action)
    {
        $action = ucfirst($action);
        //проверяем на существование в объекте необходимого экшена
        if (method_exists($this, 'action' . $action)) {
            //запускаем на выполнение необходимый экшен
            return call_user_func(array($this, 'action' . $action));
        }

        return null;
    }

    /**
     * Добавить ошибку
     *
     * @param $message
     */
    protected function addError($message)
    {
        $this->errors[] = $message;
    }

    /**
     * Перенаправление пользователя
     *
     * @param string $url
     */
    public function redirect(string $url = '/')
    {
        header('Location: ' . $url);
    }
}

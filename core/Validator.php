<?php

declare(strict_types=1);

namespace Core;

/**
 * Класс валидатора
 */
class Validator
{
    /**
     * Метод проверяет существования поля
     *
     * @param $attribute
     * @param array $params
     * @param array $details
     *
     * @return bool
     */
    public static function required($attribute, array $params, array $details): bool
    {
        if (isset($params[$attribute])) {
            return true;
        }
        return false;
    }

    /**
     * Метод проверяет, является ли переменная числом или строкой, содержащей число
     *
     * @param $attribute
     * @param array $params
     * @param array $details
     *
     * @return bool
     */
    public static function numeric($attribute, array $params, array $details): bool
    {
        if (is_numeric($params[$attribute])) {
            return true;
        }
        return false;
    }

    /**
     * Метод проверяет, является ли переменная строкой
     *
     * @param $attribute
     * @param array $params
     * @param array $details
     *
     * @return bool
     */
    public static function string($attribute, array $params, array $details): bool
    {
        if (is_string($params[$attribute])) {
            return true;
        }
        return false;
    }

    /**
     * Метод проверяет на ограничение по минимальному значению
     *
     * @param $attribute
     * @param array $params
     * @param array $details
     *
     * @return bool
     */
    public static function min($attribute, array $params, array $details): bool
    {
        if ($params[$attribute] >= $details['value']) {
            return true;
        }

        return false;
    }

    /**
     * Метод проверяет значение на соответствие регулярному выражению
     *
     * @param $attribute
     * @param array $params
     * @param array $details
     *
     * @return bool
     */
    public static function regex($attribute, array $params, array $details): bool
    {
        if (preg_match($details['pattern'], $params[$attribute])) {
            return true;
        }

        return false;
    }
}

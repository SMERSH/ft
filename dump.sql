SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


--
-- База данных: `financial_transactions_test`
--

CREATE DATABASE IF NOT EXISTS `financial_transactions_test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `financial_transactions_test`;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `login` VARCHAR(255) NOT NULL UNIQUE,
  `password_hash` VARCHAR(255) NOT NULL,
  `balance` BIGINT DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `index_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE utf8_general_ci AUTO_INCREMENT=1;

INSERT INTO users (`name`, `login`, `password_hash`, `balance`)
VALUES ('Тестовый пользователь 1', 'test', '$2y$10$NaCUj0IvAFJFErf2Z8n1seDTWZF3T/gahzxi2H5pajKi2DHbl4Dv.', 15707582),
('Тестовый пользователь 2', 'test2', '$2y$10$NaCUj0IvAFJFErf2Z8n1seDTWZF3T/gahzxi2H5pajKi2DHbl4Dv.', 13774035);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

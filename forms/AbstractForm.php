<?php

declare(strict_types=1);

namespace Forms;

use Core\Validator;

/**
 * Абстрактный класс формы
 */
abstract class AbstractForm
{
    /**
     * Массив данных
     *
     * @var array
     */
    protected $request = [];

    /**
     * Массив ошибок
     *
     * @var array
     */
    public $errors = [];

    /**
     * Получить массив правил
     *
     * @return array|array[]
     */
    abstract public function rules(): array;

    /**
     * Получить массив названий полей формы
     *
     * @return array|string[]
     */
    abstract public function getLabels(): array;

    /**
     * Получить массив сообщений об ошибках
     *
     * @return array|string[]
     */
    public function messages(): array
    {
        return [
            'required' => 'Поле "%s" обязательно для заполнения',
            'numeric'  => 'Поле "%s" должно быть числом',
            'string'   => 'Поле "%s" должно быть строкой',
            'min'      => 'Полю "%s" должно быть больше %s',
            'regex'    => 'Поле "%s" должно содержать только цифры и 2 знака после точки (пример: 123.45)'
        ];
    }


    /**
     * Получить сообщение об ошибке
     *
     * @param $rule
     * @param $attribute
     * @param array $details
     * @return string
     */
    public function getMessage($rule, $attribute, array $details)
    {
        $messages = $this->messages();
        if (array_key_exists($rule, $messages)) {
            return sprintf($messages[$rule], $this->getLabel($attribute), $details['value'] ?? '');
        }

        return 'Неизвестная ошибка';
    }

    /**
     * Заполнить массив данных
     *
     * @param array $request
     */
    public function handleRequest(array $request): void
    {
        $this->request = $request;
    }

    /**
     * Проверить отправку формы
     *
     * @return bool
     */
    public function isSubmitted(): bool
    {
        if (isset($this->request['submit'])) {
            return true;
        }

        return false;
    }

    /**
     * Метод валидации данных
     *
     * @return bool
     */
    public function isValid(): bool
    {
        foreach ($this->rules() as $details) {
            foreach ($details['attributes'] as $attribute) {
                $method = $details['rule'];

                if (Validator::$method($attribute, $this->request, $details) === false) {
                    $this->addError($this->getMessage($method, $attribute, $details));
                }
                if (count($this->errors)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Метод добавляет ошибку в массив ошибок
     *
     * @param $message
     */
    public function addError($message): void
    {
        $this->errors[] = $message;
    }

    /**
     * Получить массив ошибок
     *
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Получить параметр из реквеста
     *
     * @param $name
     * @param null $default
     * @return mixed|null
     */
    public function getParam($name, $default = null)
    {
        return $this->request[$name] ?? $default;
    }

    /**
     * Получить название атрибута
     *
     * @param string $attribute
     * @return mixed|string
     */
    public function getLabel(string $attribute)
    {
        $labels = $this->getLabels();

        return $labels[$attribute] ?? $attribute;
    }
}

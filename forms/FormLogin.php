<?php

declare(strict_types=1);

namespace Forms;

/**
 * Форма авторизации
 */
class FormLogin extends AbstractForm
{
    /**
     * Получить массив правил
     *
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            ['attributes' => ['login', 'password'], 'rule' => 'required'],
            ['attributes' => ['login', 'password'], 'rule' => 'string'],
        ];
    }

    /**
     * Получить массив названий полей формы
     *
     * @return array|string[]
     */
    public function getLabels(): array
    {
        return [
            'login'    => 'Логин',
            'password' => 'Пароль',
        ];
    }
}

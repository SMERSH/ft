<?php

declare(strict_types=1);

namespace Forms;

/**
 * Форма вывод списания денег со счета пользователя
 */
class FormWithdrawal extends AbstractForm
{
    /**
     * Получить массив правил
     *
     * @return array|array[]
     */
    public function rules(): array
    {
        return [
            ['attributes' => ['sum'], 'rule' => 'required'],
            ['attributes' => ['sum'], 'rule' => 'numeric'],
            ['attributes' => ['sum'], 'rule' => 'min', 'value' => '0.01'],
            ['attributes' => ['sum'], 'rule' => 'regex', 'pattern' => '/^\d+\.?\d{0,2}?$/'],
        ];
    }

    /**
     * Получить массив названий полей формы
     *
     * @return array|string[]
     */
    public function getLabels(): array
    {
        return [
            'sum' => 'Вывод средств',
        ];
    }

    /**
     * Получить сумму конвертируемую в копейки
     *
     * @return float|int
     */
    public function getConvertToPennies(): int
    {
        return (int)($this->getParam('sum') * 100);
    }
}

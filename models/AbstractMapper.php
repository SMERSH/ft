<?php

declare(strict_types=1);

namespace Models;

/**
 * Абстрактный класс для конвертации данных из репозитория в модель
 */
abstract class AbstractMapper
{
    /**
     * Метод для создания объекта с переданными параметрами
     *
     * @param array $row
     * @return mixed
     */
    abstract protected function rowToObject(array $row);
}

<?php

declare(strict_types=1);

namespace Models;

/**
 * Модель пользователя
 */
class User
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $login;

    /**
     * @var int
     */
    private $balance;

    /**
     * @var string
     */
    private $password_hash;

    /**
     * User constructor.
     * @param int $id
     * @param string $name
     * @param string $login
     * @param int $balance
     * @param string $password_hash
     */
    public function __construct(int $id, string $name, string $login, int $balance, string $password_hash)
    {
        $this->id = $id;
        $this->name = $name;
        $this->login = $login;
        $this->balance = $balance;
        $this->password_hash = $password_hash;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLogin(): string
    {
        return $this->login;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->password_hash;
    }

    /**
     * Получить сумму для отображения
     *
     * @return string
     */
    public function getBalanceView(): string
    {
        return number_format($this->balance / 100, 2, '.', ' ') . ';';
    }

    /**
     * Метод создает объект пользователя
     *
     * @param array $data
     * @return User
     */
    public static function create(array $data): User
    {
        return new self(
            (int)$data['id'],
            $data['name'],
            $data['login'],
            (int)$data['balance'],
            $data['password_hash']
        );
    }

    /**
     * Метод проверки пароля
     *
     * @param string $password
     *
     * @return bool
     */
    public function checkPassword(string $password): bool
    {
        return password_verify($password, $this->getPasswordHash());
    }
}

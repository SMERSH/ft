<?php

declare(strict_types=1);

namespace Models;

class UserMapper extends AbstractMapper
{
    /**
     * Метод для создания объекта с переданными параметрами
     *
     * @param array $row
     * @return User
     */
    public function rowToObject(array $row): User
    {
        return User::create($row);
    }
}

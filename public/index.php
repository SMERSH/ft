<?php
    ini_set('display_errors', true);
    error_reporting(E_ALL);

    /** @var \Core\Autoloader $autoloader */
    $autoloader = require_once '../core/Autoloader.php';

    /** @var \Core\Container $container */
    $container = new \Core\Container();
    (new \Core\Application($container))->run();

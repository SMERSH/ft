### Тестовый проект
PHP 7.2 + Mysql 5.7

Настройки подключения к БД хранятся в `configs/Config.php`

Дамп БД с двумя пользователями в корневой папке `dump.sql`

Тестовые пользователи:

`login:test` `password:test`

`login:test2` `password:test`

## Проверка кода
Команда проверки кода:

	./vendor/bin/phpcs --standard=phpcs.xml

Можно запустить phpcbf что бы исправить код автоматически (работает не во всех случаях) 
Команда автоисправления кода:

	./vendor/bin/phpcbf
    
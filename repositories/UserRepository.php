<?php

declare(strict_types=1);

namespace Repositories;

use Core\Db;
use Models\User;
use Models\UserMapper;

/**
 * Репозиторий пользователей
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @var string Название таблицы
     */
    public $table = 'users';

    /**
     * @var \PDO
     */
    private $dbh;

    /**
     * @var UserMapper
     */
    private $userMapper;

    public function __construct(Db $db, UserMapper $userMapper)
    {
        $this->dbh        = $db->getDbh();
        $this->userMapper = $userMapper;
    }

    /**
     * @return \PDO
     */
    public function getDbh()
    {
        return $this->dbh;
    }

    /**
     * @inheritDoc
     */
    public function findById(int $id, bool $block = false): User
    {
        $query = 'SELECT * FROM ' . $this->table . ' WHERE `id`=:id';
        if ($block) {
            $query .= ' FOR UPDATE';
        }
        $sth = $this->getDbh()->prepare($query);
        $sth->execute(['id' => $id]);

        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        if (!$result) {
            throw new \DomainException('User #' . $id . ' not found');
        }

        return $this->userMapper->rowToObject($result);
    }

    /**
     * @inheritDoc
     */
    public function findByLogin(string $login): User
    {
        $sth = $this->getDbh()->prepare('SELECT * FROM ' . $this->table . ' WHERE `login`=:login');
        $sth->execute(['login' => $login]);

        $result = $sth->fetch(\PDO::FETCH_ASSOC);

        if (!$result) {
            throw new \DomainException('Пользователь "' . $login . '" не найден');
        }

        return $this->userMapper->rowToObject($result);
    }

    /**
     * @inheritDoc
     */
    public function updateBalance(int $id, int $balance): bool
    {
        $sth = $this->getDbh()->prepare('
            UPDATE ' . $this->table . ' 
            SET `balance` = :balance
            WHERE `id` = :id');

        return $sth->execute([
            'id'      => $id,
            'balance' => $balance
        ]);
    }
}

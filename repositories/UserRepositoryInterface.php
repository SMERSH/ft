<?php

declare(strict_types=1);

namespace Repositories;

use Models\User;

/**
 * Интерфейс, описывающий необходимые методы для UserRepository
 */
interface UserRepositoryInterface
{
    /**
     * Метод, служит для поиска объекта по id
     *
     * @param int $id
     * @param bool $block
     *
     * @return User
     */
    public function findById(int $id, bool $block = false): User;

    /**
     * Метод, служит для поиска объекта по login
     *
     * @param string $login
     * @return User
     */
    public function findByLogin(string $login): User;

    /**
     * Метод обновления баланса пользователя
     *
     * @param int $id
     * @param int $balance
     *
     * @return bool
     */
    public function updateBalance(int $id, int $balance): bool;
}

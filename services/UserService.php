<?php

declare(strict_types=1);

namespace Services;

use Models\User;
use Repositories\UserRepositoryInterface;

/**
 * Сервис для работы с пользователями
 */
class UserService
{
    /**
     * @var UserRepositoryInterface
     */
    private $repository;

    /**
     * UserService constructor.
     * @param UserRepositoryInterface $repository
     */
    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Получить маппер
     *
     * @return UserRepositoryInterface
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Метод вычитания средств со счета пользователя
     *
     * @param int $id
     * @param int $sum
     */
    public function makeWithdrawal(int $id, int $sum): void
    {
        /** @var User $user */
        $user = $this->getRepository()->findById($id, true);

        if ($user->getBalance() < $sum) {
            throw new \DomainException('Сумма вывода средств не может быть больше баланса пользователя');
        }

        $this->getRepository()->updateBalance($id, $user->getBalance() - $sum);
    }

    /**
     * Метод авторизации пользователя
     *
     * @param string $login
     * @param string $password
     *
     * @return User
     */
    public function login(string $login, string $password): User
    {
        /** @var User $user */
        $user = $this->getRepository()->findByLogin($login);

        if ($user->checkpassword($password)) {
            return $user;
        }

        throw new \DomainException('Неверный пароль');
    }
}

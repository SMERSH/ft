<?php if(isset($errors) && count($errors)) { ?>
    <div class="alert alert-danger">
        <?php foreach ($errors as $error) { ?>
            <?= $error  . '<br>'?>
        <?php } ?>
    </div>
<?php } ?>
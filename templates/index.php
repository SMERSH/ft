<?php
use Forms\FormWithdrawal;
use Core\View;
/**
 * @var $balance
 * @var FormWithdrawal $form
 * @var array $errors
 * @var View $this
*/
?>

<h3>Денег на счету: <?= $balance ?></h3>
<?php $this->renderPartial('errors', [
        'errors' => $errors
]); ?>

<form class="form-inline" method="post">
    <div class="form-group">
        <label>Вывод средств</label>
        <input type="text" class="form-control" id="sum" name="sum" placeholder="Сумма вывода" value="<?= $form->getParam('sum') ?>">
    </div>
    <button type="submit" name="submit" class="btn btn-primary">Вывести</button>
</form>

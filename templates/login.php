<?php
use Core\View;
/**
 * @var array $errors
 * @var View $this
 */
?>
<div class="login-form">
    <form action="/main/login" method="post">
        <h2 class="text-center">Авторизация</h2>
        <?php $this->renderPartial('errors', [
            'errors' => $errors
        ]); ?>
        <div class="form-group">
                <input type="text" name="login" class="form-control" placeholder="Login" required="required">
        </div>
        <div class="form-group">
            <input type="password" name="password" class="form-control" placeholder="Password" required="required">
        </div>
        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Войти</button>
        </div>
    </form>
</div>

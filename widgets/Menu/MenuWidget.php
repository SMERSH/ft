<?php

declare(strict_types=1);

namespace Widgets\Menu;

use Widgets\Widget;

/**
 * Виджет меню
 */
class MenuWidget extends Widget
{
    /**
     * @inheritDoc
     *
     * @return string|void
     */
    public function run()
    {
        return $this->renderPartial('../widgets/Menu/template/navbar');
    }
}

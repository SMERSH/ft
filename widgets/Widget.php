<?php

declare(strict_types=1);

namespace Widgets;

use Core\View;

/**
 * Абстрактный класс виджета
 */
abstract class Widget extends View
{
    /**
     * Создает экземпляр виджета и запускает его.
     *
     * @return string результат выполнения виджета
     * @throws \Exception
     */
    public static function widget()
    {
        ob_start();
        ob_implicit_flush(0);
        try {
            $widget_class = get_called_class();
            $widget = new $widget_class();
            $out = $widget->run();
        } catch (\Exception $e) {
            if (ob_get_level() > 0) {
                ob_end_clean();
            }
            throw $e;
        }

        return ob_get_clean() . $out;
    }

    /**
     * Запускает выполнение виджета.
     *
     * @return string результат выполнения виджета
     */
    abstract public function run();
}
